from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import Perceptron
from sklearn.naive_bayes import ComplementNB
from sklearn.naive_bayes import BernoulliNB
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neighbors import KNeighborsClassifier


def doc_preprocess(document, doc_type):
    if doc_type == 'train':
        for line in document:
            statement = line.replace("\n", '').split('\t')
            labels.append(statement[0])
            train_data.append(statement[1])
    elif doc_type == 'test':
        for line in document:
            test_data.append(line)


if __name__ == '__main__':
    labels = []
    train_data = []
    ratio = 0
    with open('train/train.data', 'r') as train_file:
        doc_preprocess(train_file, "train")
    # Some magic here
    tfidf_vectorizer = TfidfVectorizer(max_df=0.90, max_features=10000)
    train_matrix = tfidf_vectorizer.fit_transform(train_data)
    test_data = []
    with open('test/test.data', 'r') as test_file:
        doc_preprocess(test_file, "test")
    # the work
    test_matrix = tfidf_vectorizer.transform(test_data)
    names = ["KNN", "RandomForest", "Perceptron", "ComplementNB", "Bernoulli"]
    # knn, random_forest, perceptron, SVC
    classifiers = [KNeighborsClassifier(n_neighbors=5), RandomForestClassifier(n_estimators=10), Perceptron(),
                   ComplementNB(), BernoulliNB()]
    # knn = KNeighborsClassifier(n_neighbors=5)
    # knn.fit(train_matrix, labels)
    for name, cf in zip(names, classifiers):
        cf.fit(train_matrix, labels)
        print("Running {}".format(name))
        with open(name + ".txt", "w") as write_file:
            i = 1
            for test_statement in test_matrix:
                res = cf.predict(test_statement)
                if res == '1':
                    print("{}".format(i))
                    i += 1
                write_file.write(res[0] + "\n")








