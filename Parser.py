import re
import numpy as np
import pickle
from sklearn.decomposition import PCA
from sklearn.decomposition import SparsePCA
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_predict
from sklearn import metrics
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import AdaBoostClassifier
from sklearn.svm import SVC
from sklearn import cross_validation, linear_model
from sklearn.linear_model import LogisticRegression


train_file = "train.dat"
test_file = "test.dat"
edr_output = "edr_output.dat"
edr_test_output = "edr_test_output.dat"
print_debug = True
feature_vector_size = 100001
num_pca_components = 1000
k_folds = 10

edr_types = ["sparsePCA"]
edr_n_components = [100]
dump_test_results = True

def preprocess(filename, mode):

    with open(filename, "r") as fh:
        lines = fh.readlines()

    if mode == "train":
        labels = [int(l[0]) for l in lines]
        for index, item in enumerate(labels):
            if item == 0:
                labels[index] = -1
        docs = [re.sub(r'[^\w]', ' ',l[1:]).split() for l in lines]

    else:
        labels = []
        docs = [re.sub(r'[^\w]', ' ',l).split() for l in lines]

    features = []
    for doc in docs:
        line = [0]*feature_vector_size
        for index, val in enumerate(doc):
            line[int(val)] = 1
        print(line)
        features.append(line)
    return features, labels

if __name__ == '__main__':

    train_file = "train/train.data"
    # Extract faetures
    features, labels = preprocess(train_file, "train")
